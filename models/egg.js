var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EggSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  price: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('Egg', EggSchema);
