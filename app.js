const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')
var session = require('express-session');
const passport = require('passport');
var MongoClient = require('mongodb').MongoClient;
var bcrypt = require('bcrypt-nodejs');

const config = require('./config/database');
var User = require("./models/user");
var Egg = require("./models/egg");

mongoose.Promise = require('bluebird');
mongoose.connect(config.database, {
  useMongoClient: true
});

// var mongoOptions = {
//   Promise: require('bluebird')
// };

MongoClient.connect(config.database, function (err, db) {

  if (err) {
    console.log(err);
    return;
  };

  let usersExtists = false;
  let eggsExtists = false;

  var dbo = db.db("eastereggprice");

  const createUsers = () => {

    let user = {
      username: "ffit",
      password: "123456"
    };

    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return
      }
      bcrypt.hash(user.password, salt, null, (err, hash) => {
        if (err) {
          console.log("error on creating user", err);
          return
        }
        user.password = hash;

        const newUser = new User(user);

        dbo.collection("users").insertOne(newUser);
        console.log("creating user");
      });
    });

  }

  const createEggs = (should) => {
    if (should) {
      console.log("creating egg");

      const eggList = [{
        name: "Ovo Alpino",
        imageUrl: "https://statics-americanas.b2w.io/produtos/01/02/special/121761332/img/ovo.png",
        price: "34.99"
      }, {
        name: "Ovo Serenata de Amor",
        imageUrl: "https://images-americanas.b2w.io/produtos/01/00/sku/32220/3/32220381_1SZ.jpg",
        price: "39.99"
      }, {
        name: "Ovo Galak",
        imageUrl: "https://images-americanas.b2w.io/spacey/2017/03/09/OVOBRANCO.jpg",
        price: "39.99"
      }, {
        name: "Ovo Frozen",
        imageUrl: "https://images-americanas.b2w.io/spacey/2017/03/16/OVODELICE.jpg",
        price: "37.99"
      }, {
        name: "Ovo Ariel",
        imageUrl: "https://1.bp.blogspot.com/-oOhrNN7oK08/WMN6Oxb1akI/AAAAAAAAG4o/eCrnxjPHrK0juusVRv5g_T8tQrzEn6KaACLcB/s1600/17191459_1128396763937438_4020650158953741598_n.jpg",
        price: "49.99"
      }, {
        name: "Ovo Chokkie",
        imageUrl: "https://geekpublicitario.com.br/wp-content/uploads/2017/04/chokkie-ovo-de-pascoa-outback-destaque-825x432.jpg",
        price: "79.99"
      }, {
        name: "Ovo Mickey",
        imageUrl: "https://i.pinimg.com/originals/64/33/f5/6433f595ac963ad7b291d74b2a5a8174.jpg",
        price: "54.99"
      }, {
        name: "Ovo de Bacon",
        imageUrl: "https://segredosdomundo.r7.com/wp-content/uploads/2017/04/destaque-20-758x455.jpg",
        price: "49.99"
      }, {
        name: "Ovo Gourmet",
        imageUrl: "https://abrilexame.files.wordpress.com/2016/09/size_960_16_9_le-vin.jpg?quality=70&strip=info&w=920",
        price: "129.99"
      }];

      const eggListMongo = [];

      for (egg of eggList) {
        var newEgg = new Egg(egg);
        eggListMongo.push(newEgg);
      }

      dbo.collection("eggs").insertMany(eggListMongo, function (err, eggs) {
        if (err) {
          return console.error(err)
        }
        console.log('Done!')
        // db.close();
      })
    } else {
      // db.close();
    }
  }

  dbo.collection("users").count((err, count) => {
    if (count == 0) {
      createUsers();
    }

  });

  dbo.collection("eggs").count((err, count) => {

    createEggs(count == 0); // if theres no eggs, it will create the collection when inserting, otherwise it will close the database, as users might be instantiated already

  });

});

var api = require('./routes/api');

var app = express();

// Express Session
app.use(session({
  secret: config.secret,
  saveUninitialized: true,
  resave: true
}));

// CORS support.
app.all('*', function(req, res, next){
  if (!req.get('Origin')) return next();
  res.set('Access-Control-Allow-Origin', '*' );
  res.set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  next();
});

// setting up middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());

// Passport init
app.use(passport.initialize());
app.use(passport.session());

app.use('/api/v1', api);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render('error');
});

module.exports = app;