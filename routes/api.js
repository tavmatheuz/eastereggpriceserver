// var express = require('express');
// var router = express.Router();

// // Implement your services here //

// module.exports = router;

var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var User = require("../models/user");
var Egg = require("../models/egg");


// APIS FOR TEST SAKE
router.post('/signup', function (req, res) {
  if (!req.body.username || !req.body.password) {
    res.status(401).json({
      success: false,
      msg: 'Please pass username and password.'
    });
  } else {
    var newUser = new User({
      username: req.body.username,
      password: req.body.password
    });
    // save the user
    newUser.save(function (err) {
      if (err) {
        return res.status(401).json({
          success: false,
          msg: 'Username already exists.'
        });
      }
      res.status(201).json({
        success: true,
        msg: 'Successful created new user.'
      });
    });
  }
});


router.post('/create-egg', function (req, res) {
  // console.log(req.body);
  var newEgg = new Egg({
    name: req.body.name,
    imageUrl: req.body.image,
    price: req.body.price
  });

  newEgg.save(function (err) {
    if (err) {
      return res.status(400).json({
        success: false,
        msg: 'Save egg failed.'
      });
    }
    res.status(201).json({
      success: true,
      msg: 'Successful created new egg.'
    });
  });
});

// REQUIRED APIS
router.post('/auth', function (req, res) {
  User.findOne({
    username: req.body.username
  }, function (err, user) {
    if (err) throw err;

    if (!user) {
      res.status(401).send({
        success: false,
        msg: 'Authentication failed. User not found.'
      });
    } else {
      // check if password matches
      user.comparePassword(req.body.password, function (err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          var token = jwt.sign(user, config.secret);
          // return the information including token as JSON
          res.json({
            success: true,
            token: 'JWT ' + token
          });
        } else {
          res.status(401).send({
            success: false,
            msg: 'Authentication failed. Wrong password.'
          });
        }
      });
    }
  });
});

router.delete('/auth', function (req, res) {
  
  req.logOut();

  req.session.destroy();
  
  req.session = null;

  

  res.send({
    success: true,
    msg: 'Logged out successfully.'
  });

});

router.get('/easter-eggs', passport.authenticate('jwt', {
  session: false
}), function (req, res) {
  // console.log("easteregg",req.headers);
  var token = getToken(req.headers);
  if (token) {
    Egg.find(function (err, eggs) {
      if (err) return next(err);
      res.json(eggs);
    });
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

router.get('/easter-eggs/:id', passport.authenticate('jwt', {
  session: false
}), function (req, res) {
  var token = getToken(req.headers);
  if (token) {
    // console.log('id', req.params.id);
    Egg.findOne({
      '_id': req.params.id
    }, function (err, egg) {
      // console.log("_id   :" + egg[0].id);
      // console.log("name:" + egg.name);
      if (err) return next(err);
      res.json(egg);
    })
  } else {
    return res.status(403).send({
      success: false,
      msg: 'Unauthorized.'
    });
  }
});

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;